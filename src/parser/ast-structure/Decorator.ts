import {Identifier} from "./root-types";

type Decorator = Identifier;
export default Decorator;
